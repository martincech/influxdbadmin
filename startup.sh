#!/bin/bash
for f in *.in; do cat $f | envsubst > `basename $f .in`; done

ng build
exec node server.js
