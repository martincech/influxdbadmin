import { Component, OnInit, OnDestroy } from '@angular/core';
import { InfluxService } from '../influx.service';
import { ActivatedRoute } from '@angular/router';
import { MessageService } from '../message.service';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-db-items',
  templateUrl: './db-items.component.html',
  styleUrls: ['./db-items.component.css']
})
export class DbItemsComponent implements OnInit {

  public rpName: string;
  public rpDuration: number = 1;
  
  public rpDefault: boolean = false;
  public rpDurations = [
    {id: 1, name: "hours", shortcut: "h"},
    {id: 2, name: "minutes", shortcut: "m"},
    {id: 3, name: "days", shortcut: "s"}
  ];
  public rpDurationUnits: any = this.rpDurations[0];
  public rpReplications = [
    {id: "1"},
    {id: "2"},
    {id: "3"},
    {id: "4"}
  ];
  public rpReplication: any = this.rpReplications[0];

  database: string;
  type: string;
  items: any = [];
  count: number = 0;
  private sub: any;

  constructor(private influxService: InfluxService, private route: ActivatedRoute, private messageService: MessageService) { }
  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.database = params['database'];
      this.type = params['type'];
      this.influxService.setView(this.database, this.type);
      this.update();
    });
  }

  private update() {
    this.influxService.getItems().subscribe(items => {
      if (this.type == "cqueries") items = items.filter(f => f.query.indexOf(this.database) != -1);
      this.items = items;
      this.count = items.length;
    });
  }

  getString(item) {
    return JSON.stringify(item);
  }

  delete(name) {
    this.influxService.delete(name).subscribe(result => {
      this.items = this.items.filter(function (item) { return item.name != name });
      this.count = this.items.length;
    });
  }

  deleteAll() {
    this.messageService.setConfirmation("Delete all ?",
      () => {
        this.items.forEach(element => {
          this.delete(element.name);
        });
      },
      () => { }
    )
  }

  createRp(){
    this.influxService
    .createRp(this.rpName, this.rpDuration + this.rpDurationUnits.shortcut, this.rpReplication.id, this.rpDefault)
    .subscribe(r => { 
      this.update();
      this.rpName = "";
      this.rpDefault = false;    
      this.rpDuration = 1;
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
