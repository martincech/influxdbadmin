import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DbItemsComponent } from './db-items.component';

describe('DbItemsComponent', () => {
  let component: DbItemsComponent;
  let fixture: ComponentFixture<DbItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DbItemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DbItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
