import { Component, OnInit, Input } from '@angular/core';
import { InfluxService } from '../influx.service';

@Component({
  selector: 'app-db-values',
  templateUrl: './db-values.component.html',
  styleUrls: ['./db-values.component.css']
})
export class DbValuesComponent implements OnInit {

  private allItems: any[] = [];

  @Input()
  table: string = null;
  @Input()
  samples: boolean = false;
  items: any[] = [];

  paging: any = { page: 0, count: 15, max: 0 };
  constructor(private influxService: InfluxService) { }

  ngOnInit() {
    this.influxService.getValues(this.table).subscribe(items => {
      this.paging.max = items.length;
      this.allItems = items;
      this.goTo(0);
    });
  }
  getString(item) {
    return JSON.stringify(item);
  }

  goTo(page) {
    this.paging.page = page;
    this.items = this.allItems.slice(this.paging.page * this.paging.count, (this.paging.page + 1) * this.paging.count);
  }

  first() {
    this.goTo(0);
  }

  last() {
    var last = Math.floor(this.paging.max / this.paging.count);
    this.goTo(last)
  }

  delete(time) {
    this.influxService.deleteValue(this.table, time).subscribe(r => {
      this.allItems = this.allItems.filter(f => f.time != time);
      this.goTo(this.paging.page);
    });
  }
}
