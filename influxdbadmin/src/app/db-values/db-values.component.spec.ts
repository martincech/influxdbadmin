import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DbValuesComponent } from './db-values.component';

describe('DbValuesComponent', () => {
  let component: DbValuesComponent;
  let fixture: ComponentFixture<DbValuesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DbValuesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DbValuesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
