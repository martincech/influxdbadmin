import { Component, OnInit, OnDestroy } from '@angular/core';
import { InfluxService } from '../influx.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

  private _type: string = null;
  private sub: any;
  database: string = null;

  constructor(private influx: InfluxService) { }

  ngOnInit() {
    this.influx.database.subscribe(params => {
      this.database = params["database"];
      this._type = params["type"];  
    });
  }

  isActive(type) {
    return this._type == type;
  }
}
