import { Component, OnInit } from '@angular/core';
import { InfluxService } from '../influx.service';

@Component({
  selector: 'app-databases',
  templateUrl: './databases.component.html',
  styleUrls: ['./databases.component.css']
})
export class DatabasesComponent implements OnInit {
  dbName: string = ""
  private _database: string = null;
  type: string = "query";
  databases: any = [];
  visible: any = {};

  constructor(private influxService: InfluxService) { }

  ngOnInit() {  
    this.influxService.databases.subscribe(databases => {
      this.databases = databases;     
    });

    this.influxService.database.subscribe(params => {
      this.type = params["type"] || "query";
      this._database = params["database"];
     });
     this.influxService.getDatabases();
  }

  isActive(database){
    return this._database == database;
  }

  createDb(){
    this.influxService.createDb(this.dbName).subscribe(result => { });
  }
}