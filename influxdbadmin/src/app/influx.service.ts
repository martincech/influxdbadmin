import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class InfluxService {
  private _subject = new BehaviorSubject({});
  public database = this._subject.asObservable();

  private _queryHistory = new BehaviorSubject([]);
  public queryHistory = this._queryHistory.asObservable();
  
  private _databases = new BehaviorSubject([]);
  public databases = this._databases.asObservable();

  constructor(private http: Http) {
    this.getQuery();
  }

  setView(database, type) {
    this._subject.next({ database: database, type: type });
  }

  //DATABASES
  getDatabases() {
    this.http.get('/api/')
      .map(res => {
        return res.json()
      }).subscribe(db =>{
        this._databases.next(db)
      });
  };

  //ITEMS
  getItems() {
    var database = this._subject.getValue()["database"];
    var type = this._subject.getValue()["type"];
    return this.http.get('/api/db/' + database + "/" + type)
      .map(res => res.json());
  };

  updateItems() {
    var database = this._subject.getValue()["database"];
    var type = this._subject.getValue()["type"];
    return this.http.get('/api/db/' + database + "/" + type)
      .map(res => res.json());
  };

  delete(name) {
    var database = this._subject.getValue()["database"];
    var type = this._subject.getValue()["type"];
    return this.http.post('/api/db/' + database + "/delete/item", { type: type, name: name })
      .map(res => res.json());
  }

  // TABLE VALUES
  getValues(table) {
    var database = this._subject.getValue()["database"];
    return this.http.get('/api/db/' + database + "/select/" + table)
      .map(res => res.json());
  };

  deleteValue(table, time) {
    var database = this._subject.getValue()["database"];
    return this.http.post('/api/db/' + database + "/delete/value", { table: table, time: time })
      .map(res => res.json());
  }

  createDb(db){
    var database = db;
    return this.http.post('/api/db/query/create', { database: db })
      .map(res => {
        var history = this._queryHistory.getValue();
        var data = res.json();
        var item = { timestamp: new Date(), req: "create database "+db, res: {results : data.results}, _id: data._id, database: database };
        history.unshift(item);
        console.log(history);
        this._queryHistory.next(history);
        this.getDatabases();
        return res.json()
      }); 
  }

  //RAW QUERY
  rawQueryHistory(query) {
    var database = this._subject.getValue()["database"];
    return this.http.post('/api/db/' + database + "/query", { query: query })
      .map(res => {
        var history = this._queryHistory.getValue();
        var data = res.json();
        var item = { timestamp: new Date(), req: query, res: {results : data.results}, _id: data._id, database: database };
        history.unshift(item);
        console.log(history);
        this._queryHistory.next(history);
        return res.json()
      });
  }

  getQuery() {
    return this.http.get('/api/db/query').map(res => {
      var queries = res.json();
      if (queries == null) return [];
      queries.reverse();
      var history = queries.map(m => { return { req: m.query, res: { results: m.results }, _id: m._id, timestamp: m.timestamp, database: m.database } });
      this._queryHistory.next(history);
      return history;
    });
  }

  deleteQuery(_id) {
    return this.http.post('/api/db/query/delete', { _id: _id }).map(res => {
      if (res.json().success) {
        var history = this._queryHistory.getValue();
        history = history.filter(f => f._id != _id);
        this._queryHistory.next(history);
      }
      return history;
    });
  }

  clearQuery() {
    return this.http.post('/api/db/query/clear', {}).map(res => {
      if (res.json().success) this._queryHistory.next([]);
      console.log(res);
      return [];
    });
  }


  // Retention policies
  defaultRp(name,duration,replication) {
    this.delete(name);
    this.createRp(name,duration, replication,true)
  }

  createRp(name,duration,replication,def) {
    var database = this._subject.getValue()["database"];
    var type = this._subject.getValue()["type"];
    return this.http.post('/api/cr/' + database + "/create/item", 
    { type: type, 
      name: name,
      duration:duration,
      replication:replication,
      default: def
    })
      .map(res => res.json())
  }
}