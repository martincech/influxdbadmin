import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { InfluxService } from './influx.service';

import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { DatabasesComponent } from './databases/databases.component';
import { DatabaseInfoComponent } from './database-info/database-info.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { DbItemsComponent } from './db-items/db-items.component';
import { DbValuesComponent } from './db-values/db-values.component';
import { FullTextSearchPipe } from './full-text-search.pipe';
import { MessageComponent } from './message/message.component';
import { MessageService } from './message.service';

const ROUTES = [
  {
    path: '',
    redirectTo: 'query',
    pathMatch: 'full'
  },
  {
    path: 'query',
    data: { type: "query" },
    component: DatabaseInfoComponent
  },
  {
    path: 'db/:database/query',
    data: { type: "query" },
    component: DatabaseInfoComponent
  },
  {
    path: 'db/:database/:type',
    component: DbItemsComponent
  },
  {
    path: '*',
    component: DatabaseInfoComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    DatabasesComponent,
    DatabaseInfoComponent,
    NavBarComponent,
    DbItemsComponent,
    DbValuesComponent,
    FullTextSearchPipe, 
    MessageComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(ROUTES)
  ],
  providers: [InfluxService, MessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
