import { Component, OnInit } from '@angular/core';
import { InfluxService } from '../influx.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-database-info',
  templateUrl: './database-info.component.html',
  styleUrls: ['./database-info.component.css']
})
export class DatabaseInfoComponent implements OnInit {
  query: string = "";
  queryHistory: any[] = [];

  constructor(private influxService: InfluxService, private route: ActivatedRoute) { }
  ngOnInit() {
    this.route.params.subscribe(params => {
      this.influxService.setView(params['database'], "query");
    });
    this.influxService.queryHistory.subscribe(s => this.queryHistory = s);
    this.influxService.getQuery().subscribe(r => { });
  }

  runQuery() {
    this.influxService.rawQueryHistory(this.query).subscribe(result => { });
  }

  queryChange() {
    this.query = this.query.replace(/\\/g, "")
  }

  getString(result) {
    return JSON.stringify(result);
  }


  delete(query) {
    this.influxService.deleteQuery(query._id).subscribe(result => { });
  }

  clear() {
    this.influxService.clearQuery().subscribe(result => { });
  }

}
