var config = {};
config.influx = {};
config.influx.host = "localhost";
config.kafka= {};
config.kafka.host = "localhost";
config.nedb= {};
config.nedb.query = "db/query.db";

module.exports = config;