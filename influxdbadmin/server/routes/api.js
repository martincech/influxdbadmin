const express = require('express');
const router = express.Router();
const Influx = require('influx');
const config = require('../../config');

const influx = new Influx.InfluxDB({
    host: config.influx.host,
    database: '_internal',
    schema: [
        {
            measurement: 'response_times',
            fields: {
                path: Influx.FieldType.STRING,
                duration: Influx.FieldType.INTEGER
            },
            tags: [
                'host'
            ]
        }
    ]
});

const Datastore = require('nedb-core');
const nedb = new Datastore({ filename: config.nedb.query, autoload: true });
nedb.persistence.setAutocompactionInterval(600000);

router.get('/', (req, res) => {
    influx.getDatabaseNames().then(names => res.send(names));
});

function wrap(items) {
    if (items == null) return items;
    return items.map(function (item) { return { name: item } });
}

router.get('/db/query', (req, res) => {
    res.send(nedb.getAllData());
});

router.get('/db/:database/select/:table', (req, res) => {
    if (req.params.table == null) {
        res.send("");
        return;
    }

    var pref = "\"1440_m\""
    if (req.params.table.endsWith("30m")) pref = "\"30_m\"";
    if (req.params.database === "samples") pref = "\"raw\"";
    var q = "select * from " + pref + ".\"" + req.params.table + "\" ORDER BY time DESC LIMIT 10000";
    influx.query(q, { database: req.params.database }).then(result => res.send(result));
});

router.post('/db/:database/delete/value', (req, res) => {
    if (req.body.table == null || req.body.table === "") {
        res.send("");
        return;
    }

    var query = "DELETE FROM \"" + req.body.table + "\" WHERE time = '" + req.body.time + "'";
    influx.queryRaw(query, { database: req.params.database })
        .then(result => res.send(result), result => res.send(result))
        .catch(result => {
            res.send({ results: [{ error: "Invalid query." }] });
        });
});

router.post('/cr/:database/create/item', (req, res) => {
    if (req.body.name == null || req.body.name === "") {
        res.send("");
        return;
    }
    switch (req.body.type) {
        case "measurements":
            break;
        case "series":
            break;
        case "retention":
            influx.createRetentionPolicy(req.body.name, {
                database: req.params.database,
                duration: req.body.duration,
                replication: req.body.replication,
                isDefault: req.body.default
            }).then(result => res.send(result))
             break;
        case "cqueries":
            break;
        default:
            res.send("");
            break;
    }
});

router.get('/db/:database/:type', (req, res) => {
    switch (req.params.type || "measurements") {
        case "measurements":
            influx.getMeasurements(req.params.database).then(names => res.send(wrap(names)));
            break;
        case "series":
            influx.getSeries({ database: req.params.database }).then(names => res.send(wrap(names)));
            break;
        case "cqueries":
            influx.showContinousQueries(req.params.database).then(names => res.send(names));
            break;
        case "retention":
            influx.showRetentionPolicies(req.params.database).then(names => res.send(names));
            break;
        default:
            influx.getMeasurements(req.params.database).then(names => res.send(wrap(names)));
            break;
    }
});

router.post('/db/:database/delete/item', (req, res) => {
    if (req.body.name == null || req.body.name === "") {
        res.send("");
        return;
    }
    switch (req.body.type) {
        case "measurements":
            influx.dropMeasurement(req.body.name, req.params.database).then(result => res.send(result));
            break;
        // case "series":
        //     break;
        case "retention":
            influx.dropRetentionPolicy(req.body.name, req.params.database).then(result => res.send(result))
             break;
        case "cqueries":
            influx.dropContinuousQuery(req.body.name, req.params.database).then(result => res.send(result))
            break;
        default:
            res.send("");
            break;
    }
});



router.post('/db/query/delete', (req, res) => {
    if (req.body._id == null) return res.send({ success: false });
    nedb.remove(req.body, {}, function (err, numRemoved) {
        var success = numRemoved == 1 ? true : false;
        res.send({ success: success });
    });
});

router.post('/db/query/clear', (req, res) => {
    nedb.remove({}, { multi: true }, function (err, numRemoved) {
        res.send({ success: true });
    });
});

router.post('/db/:database/query', (req, res) => {
    if (req.params.database == null || req.body.query == null) return res.send("");

    var q = {
        database: req.params.database,
        query: req.body.query,
        timestamp: new Date(),
        results: [{}]
    }

    nedb.insert(q, (e, d) => {
        influx.queryRaw(req.body.query, { database: req.params.database })
            .then(result => {
                result._id = d._id;
                res.send(result)
            }, result => {
                result._id = d._id;
                res.send(result)
            })
            .catch(result => {
                q.results = [{ error: "Invalid query." }];
                nedb.update({ _id: d._id }, q, {}, (err, numReplaced) => { });
                res.send({ results: q.results, _id: d._id });
            });
    });
});

router.post('/db/query/create', (req, res) => {
    influx.createDatabase(req.body.database)
        .then(result => res.send(result), result => res.send(result))
        .catch(result => {
            res.send({ results: [{ error: "Invalid query." }] });
        });

});
module.exports = router;