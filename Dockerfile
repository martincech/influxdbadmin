FROM node

RUN apt update \
  && apt -y install gettext-base \
  && rm -rf /var/lib/apt/lists/*

ARG INFLUX_HOST=influxdb
ENV INFLUX_HOST=${INFLUX_HOST}
ARG INFLUX_PORT=8086
ENV INFLUX_PORT=${INFLUX_PORT}

ADD influxdbadmin influxdbadmin
WORKDIR influxdbadmin
RUN yarn global add @angular/cli && npm install
ADD startup.sh startup.sh
RUN chmod +x startup.sh

EXPOSE 3000
CMD [ "/influxdbadmin/startup.sh" ]